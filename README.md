Depth Tools Package
=============================

Package contains any tools that are helpful when working with depth data. At the moment it contains:

- A service for getting a pose related to the object's centroid
- A service for getting a grasp pose based on the object's centroid

