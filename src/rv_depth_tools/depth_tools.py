import cv2
import numpy as np
import rospy
import tf2_geometry_msgs

import geometry_msgs.msg as geometry_msgs
import sensor_msgs.msg as sensor_msgs
from sensor_msgs.msg import PointCloud2
import std_msgs.msg as std_msgs


def calc_bounding_box(image):
    # Returns the bounding box of the points in a binary image???
    im2, contours, hierarchy = cv2.findContours(image, 1, 2)

    bestContour = contours[0]
    maxVal = 0

    rect = cv2.minAreaRect(bestContour)
    box = cv2.boxPoints(rect)
    box = np.int0(box)

    return box


def centroid_xyz_from_masked_depth(depth_image, mask_image, depth_camera_info,
                                   cv_bridge):
    # NOTE: this also returns suggested grasp width...

    # Get cv images from the ROS msg input
    # TODO would love to get this out of cv & use "ros_numpy"
    depth = cv_bridge.imgmsg_to_cv2(depth_image, "32FC1")
    mask = cv_bridge.imgmsg_to_cv2(mask_image, "mono8")

    # Create a masked depth image
    masked_depth = (mask == 255) * depth
    # masked_depth_image[np.isnan(masked_depth_image)]=0

    # Get xyz positions from the masked depth
    xyz_positions = depth_to_points(masked_depth, depth_camera_info)

    # Get the centroid from the xyz positions (either by average or median
    centroid_fn = np.median  # np.average is another option?
    centroid = [
        centroid_fn(xyz_positions[:, 0]),
        centroid_fn(xyz_positions[:, 1]),
        centroid_fn(xyz_positions[:, 2])
    ]
    # rospy.logwarn(centroid)

    # Return the centroid (numpy vector) & a dirty attempt at width
    return centroid, np.ptp(xyz_positions[:, 0])

def xyz_array_from_masked_depth(depth_image, mask_image, depth_camera_info,
                                   cv_bridge):
    # NOTE: this also returns suggested grasp width...

    # Get cv images from the ROS msg input
    # TODO would love to get this out of cv & use "ros_numpy"
    depth = cv_bridge.imgmsg_to_cv2(depth_image, "32FC1")
    mask = cv_bridge.imgmsg_to_cv2(mask_image, "mono8")

    # Create a masked depth image
    masked_depth = (mask == 255) * depth
    # masked_depth_image[np.isnan(masked_depth_image)]=0

    # Get xyz positions from the masked depth
    xyz_positions = depth_to_points(masked_depth, depth_camera_info)

    # Return the centroid (numpy vector) & a dirty attempt at width
    return xyz_positions


def depth_to_points(depth_image, depth_camera_info):
    # Get camera parameters & image shape
    cam_K = np.array(depth_camera_info.K).reshape((3, 3))
    fx = cam_K[0, 0]
    fy = cam_K[1, 1]
    cx = cam_K[0, 2]
    cy = cam_K[1, 2]

    imh, imw = depth_image.shape

    # Form a list of xyz points
    xyz_list = []
    for px in range(imw - 1):
        for py in range(imh - 1):

            x = (px - cx) * depth_image[py, px] / fx
            y = (py - cy) * depth_image[py, px] / fy
            z = depth_image[py, px]

            if not (np.isnan(z) or z == 0):
                xyz_list.append([x, y, z])

    # Return a numpy array of the xyz positions
    xyz_positions = np.array(xyz_list)
    return xyz_positions


def offset_xyz(xyz, offset):
    # We interpret "offset" is the distance an object is away from us (i.e. a
    # positive offset means SUBTRACTING from xyz)
    np_xyz = np.array(xyz)
    return list(np_xyz - offset * np_xyz / np.linalg.norm(np_xyz))

def xyz_array_to_pointcloud2(points, stamp=None, frame_id=None):
    '''
    Create a sensor_msgs.PointCloud2 from an array
    of points.
    '''
    msg = PointCloud2()
    if stamp:
        msg.header.stamp = stamp
    if frame_id:
        msg.header.frame_id = frame_id
    if len(points.shape) == 3:
        msg.height = points.shape[1]
        msg.width = points.shape[0]
    else:
        msg.height = 1
        msg.width = len(points)
    msg.fields = [
        PointField('x', 0, PointField.FLOAT32, 1),
        PointField('y', 4, PointField.FLOAT32, 1),
        PointField('z', 8, PointField.FLOAT32, 1)]
    msg.is_bigendian = False
    msg.point_step = 12
    msg.row_step = 12*points.shape[0]
    msg.is_dense = int(np.isfinite(points).all())
    msg.data = np.asarray(points, np.float32).tostring()

    return msg 

def xyz_to_pointcloud(xyz_positions, xyz_frame_id):
    # Create a point cloud message & fill in the header
    pointcloud = sensor_msgs.PointCloud(
        header=std_msgs.Header(stamp=rospy.Time.now(), frame_id=xyz_frame_id))

    # Fill in the message with points from xyz_positions (shape is (3, n))
    for point in xyz_positions:
        pointcloud.points.append(
            geometry_msgs.Point32(point[0], point[1], point[2]))

    # Return the populated pointcloud message
    return pointcloud


def xyz_to_pose(tf_buffer, xyz, xyz_frame, pose_frame='base_link',orientation_frame='patrolbot_end_effector'):
    # TODO there are dirty fudge factors in here that really need to go!!!

    # Create an orientation that faces from the object to the xyz_frame
    # TODO for now it just faces direction of camera...

    # Turn the xyz into a stamped pose
    in_pose = geometry_msgs.PoseStamped(
        header=std_msgs.Header(stamp=rospy.Time.now(), frame_id=xyz_frame),
        pose=geometry_msgs.Pose(
            position=geometry_msgs.Vector3(*xyz),
            orientation=geometry_msgs.Quaternion(
                0, 0, 0, 1)))
                # 0.707,
                # 0,
                # 0.707,
                # 0)))

    # Transform the the xyz pose into the target frame
    tf = tf_buffer.lookup_transform(pose_frame, xyz_frame, rospy.Duration(0.0))
    pose = tf2_geometry_msgs.do_transform_pose(in_pose, tf)

    #this lookups the orientation_frame in the pose_frame and
    #sets the rotation component of this transform to the xyz_pose
    tf_orientation = tf_buffer.lookup_transform(pose_frame, orientation_frame, rospy.Duration(0.0))
    pose.pose.orientation = tf_orientation.transform.rotation

    # Return the transformed pose
    return pose

def uv_to_xyz(depth_image, depth_camera_info, px, py):
    cam_K = np.array(depth_camera_info.K).reshape((3, 3))
    fx = cam_K[0, 0]
    fy = cam_K[1, 1]
    cx = cam_K[0, 2]
    cy = cam_K[1, 2]

    x = (px - cx) * depth_image[py, px] / fx
    y = (py - cy) * depth_image[py, px] / fy
    z = depth_image[py, px]

    return (x,y,z)

def xyz_to_uv(depth_camera_info, x, y, z):
    cam_K = np.array(depth_camera_info.K).reshape((3, 3))
    fx = cam_K[0, 0]
    fy = cam_K[1, 1]
    cx = cam_K[0, 2]
    cy = cam_K[1, 2]

    u = x * fx / z + cx
    v = y * fy / z + cy

    return u, v

